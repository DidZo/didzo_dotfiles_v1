" NICE and CLEAN Vim configuration file

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set autoindent

set relativenumber
set nu
set nohlsearch
set incsearch
set hidden
set noerrorbells
set nowrap

set noswapfile
set nobackup

set scrolloff=8
set termguicolors
set noshowmode
set completeopt=menuone,noinsert,noselect
set signcolumn=yes

set updatetime=50
set shortmess+=c

call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'preservim/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'uiiaoo/java-syntax.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

colorscheme gruvbox
highlight Normal guibg=none

let mapleader = " "


"General Navigation #begin
    "Move between windows
    map <leader>j <C-W>j
    map <leader>k <C-W>k
    map <leader>h <C-W>h
    map <leader>l <C-W>l
    " Close the current buffer
    map <leader>bd :bd<cr>
    " Close all the buffers
    map <leader>ba :bufdo bd<cr>
    map <C-l> :bnext<cr>
    map <C-h> :bprevious<cr>
    map <leader>/ :noh<cr>
    " Managing tabs
    map <leader>tn :tabnew<cr>
    map <leader>to :tabonly<cr>
    map <leader>tc :tabclose<cr>
    map <leader>tm :tabmove 
    map <leader>tt :tabnext
    map <leader>tp :tabprevious
    "quick save and quick quit
    nmap <leader>w :w!<cr>
    nmap <leader>q :q<cr>
"General Navigation #end 

         

"NERDTree #begin

    "nnoremap <leader>nn :NERDTreeToggle<CR>
    "nnoremap <leader>n/ :NERDTreeFind<CR>
    "nnoremap <leader>nf :NERDTreeFocus<CR>

    " Start NERDTree when Vim is started without file arguments.
    "autocmd StdinReadPre * let s:std_in=1
    "autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

    " Start NERDTree when Vim starts with a directory argument.
    "autocmd StdinReadPre * let s:std_in=1
    "autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    "    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif

    "let NERDTreeAutoDeleteBuffer = 1
    "let NERDTreeMinimalUI = 1
    "let NERDTreeDirArrows = 1
"NERDTree #end


"CoCExplorer ++
    let g:coc_explorer_global_presets = {
\   '.vim': {
\     'root-uri': '~/.vim',
\   },
\   'tab': {
\     'position': 'tab',
\     'quit-on-open': v:true,
\   },
\   'floating': {
\     'position': 'floating',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingTop': {
\     'position': 'floating',
\     'floating-position': 'center-top',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingLeftside': {
\     'position': 'floating',
\     'floating-position': 'left-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingRightside': {
\     'position': 'floating',
\     'floating-position': 'right-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'simplify': {
\     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
\   }
\ }

nmap <leader>e :CocCommand explorer<CR>
nmap <leader>f :CocCommand explorer --preset floating<CR>
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif
