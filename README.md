# DidZo dotfiles V1



[TOC]

## Version Info

- `dwm` 6.2

- `dmenu` 5.0

- `st` 0.8.4

- `slock`1.4 

  

## Dependencies

- `yay`  (Arch)
- `make`
- `nitrogen`
- `lightdm`
- `python`
- `pywal`
- `picom` git next branch (added blur)
- `nnn`

nnn :
- tabbed
- mpv
- sxiv
- zathura
- file
- mktemp
- xdotool

> Dependencies can be installed with a script, see `Installation` section



## Screenshots

![Screen Capture](./config/resources/screenshot1.png)

- `Neofetch` in `st`



![](./config/resources/screenshot2.png)

- `Spotify-tui`
- `Cavar`
- `Pulsemixer`



> Format : 21:9
>
> Resolution : 3440x1440



## Features



### dwm

- Alpha patch
- Systray patch
- Alternativetags patch
- Fullgaps patch
- Urg-border parch
- Pywal colors
- Systray+Alpha patch error fix

### st

- Alpha patch
- Anysize patch

### dmenu

- stock config

### picom

- terminal transparency when not focused (alternative to borders)
- dual-kawase (multi-threaded) blur effect

### pywal

- .cache colors edited to match with alpha patch



## Installation

Clone the project where you want :

```bash
git clone https://gitlab.com/DidZo/didzo_dotfiles_v1.git
```

Then you can edit the dot files like you want or you can use the default configuration.



### Quick Install

You can run a quick install with the following command :

```
./quickInstall.sh
```

The following stuff will be done :

- Installing the tools for the ricing
- Installing the dot files as symbolic links to the repository you cloned
- Setup `lightdm` and an autostart script
- Compiling suckless tools

> You should be ready to go



### Installing dot files only

A script is available to allow you to install the dot files automatically by creating symbolic links :

```bash
./installDotFiles.sh
```

> This is optional, you can do it manually if you don't want to rely on the git repository or if you want only a part of the configuration



### Installing required ricing tools

A script is available to allow you to install the tools configured in the dot files :

```
./installDeps.sh
```

> NOTE : On ArchLinux only



### Compiling suckless tools

You can install my `dwm/st/dmenu/slock` configuration by launching another script :

```bash
./config/install.sh
```

> NOTE : This will also create `dwm.desktop` in the xsession folder, as well as an autostart script. If you already ran it at the beginning you should consider running the next script : `reload.sh`

If you decide to change settings directly in the folder, you can then launch another script to reload `dwm/st/dmenu/slock` configuration

```
./config/reload.sh
```



## Usage

> TODO



## TODO

- fonts
- dwm blocks
- systray rice
- spotify config
