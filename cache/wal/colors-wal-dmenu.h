static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#a3dfee", "#1c0124" },
	[SchemeSel] = { "#a3dfee", "#5D5BAA" },
	[SchemeOut] = { "#a3dfee", "#5EA0DD" },
};
