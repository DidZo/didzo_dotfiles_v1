# create .config
[ ! -d $HOME/.config ] && mkdir $HOME/.config

# required deps
sudo pacman -S --needed git base-devel

path=$HOME/.config
cd $path
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd $path

paru -S --needed python nitrogen neovim picom-git python-pywal

