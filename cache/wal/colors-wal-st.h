const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#1c0124", /* black   */
  [1] = "#5D5BAA", /* red     */
  [2] = "#236ACF", /* green   */
  [3] = "#A55B9B", /* yellow  */
  [4] = "#AF53AB", /* blue    */
  [5] = "#2B97E1", /* magenta */
  [6] = "#5EA0DD", /* cyan    */
  [7] = "#a3dfee", /* white   */

  /* 8 bright colors */
  [8]  = "#729ca6",  /* black   */
  [9]  = "#5D5BAA",  /* red     */
  [10] = "#236ACF", /* green   */
  [11] = "#A55B9B", /* yellow  */
  [12] = "#AF53AB", /* blue    */
  [13] = "#2B97E1", /* magenta */
  [14] = "#5EA0DD", /* cyan    */
  [15] = "#a3dfee", /* white   */

  /* special colors */
  [256] = "#1c0124", /* background */
  [257] = "#a3dfee", /* foreground */
  [258] = "#a3dfee",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
