static const char norm_fg[] = "#a3dfee";
static const char norm_bg[] = "#1c0124";
static const char norm_border[] = "#729ca6";

static const char sel_fg[] = "#a3dfee";
static const char sel_bg[] = "#236ACF";
static const char sel_border[] = "#a3dfee";

static const char urg_fg[] = "#a3dfee";
static const char urg_bg[] = "#5D5BAA";
static const char urg_border[] = "#5D5BAA";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
