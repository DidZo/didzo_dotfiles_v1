static const char* selbgcolor   = "#1c0124";
static const char* selfgcolor   = "#a3dfee";
static const char* normbgcolor  = "#236ACF";
static const char* normfgcolor  = "#a3dfee";
static const char* urgbgcolor   = "#5D5BAA";
static const char* urgfgcolor   = "#a3dfee";
